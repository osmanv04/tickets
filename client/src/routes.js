import React from 'react'
import { Route } from 'react-router-dom'
import Login from './containers/Login'
import TicketList from './containers/TicketList'
import Home from './components/Home'
import TicketForm from './containers/TicketForm'
import Register from './containers/Register'

const BaseRouter = () => (
    <div>
        <Route exact path="/" component={Home}></Route>
        <Route exact path="/login/" component={Login}/>
        <Route exact path="/signup/" component={Register}/>
        <Route exact path="/tickets/" component={TicketList}/>
        <Route exact path="/tickets/update/:id" component={TicketForm}/>
    </div>
)

export default BaseRouter