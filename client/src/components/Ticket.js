import { Table, Button, Row, Col } from 'antd';
import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom'

class Tickets extends React.Component {

  handleCreateTicket = () => {
    axios.post('http://localhost:8080/api/ticket/create', {name: null, id: parseInt(localStorage.getItem("id"))})
        .then(resp => {
            alert("Ticket pedido!!");
        });
  }

  handleSetTicket = (id) => {
    axios.post('http://localhost:8080/api/ticket/update', {name: 'ticket 1', id: id})
    .then(resp => {
        alert("ticket seteado");
    })
      
  }

    render(){

      let columns = []
      if(this.props.data){
        columns = this.props.id === 1 ? [
          {
            title: 'Usuario',
            key: 'name',
            render: value => `${value.User.name}`
          },
          {
            title: 'Accion',
            key: 'accion',
            render: value => (
              <Link to={`/tickets/update/${value.id}`}>
                Asignar
              </Link>
            ),
          }
        ] 
        :
        [
          {
            title: 'Titulo',
            dataIndex: 'ticket_order',
            key: 'ticket_order',
          }
        ];
      }

      return (
        <div>
          { 
            this.props.id === 1 ? 
            <h1>Solicitudes de Tickets</h1> 
                    : 
            <Row justify="space-around" align="middle">
              <Col span={6} key="1">
                  <h1>Tickets</h1>
              </Col> 
              <Col span={6} key="2">
                <Button shape="round" type="primary" onClick={ (event) => this.handleCreateTicket()}>Solicitar Ticket</Button>
              </Col>              
            </Row>
          }
          <Table columns={columns} bordered dataSource={this.props.data} />
        </div>
      );
    }
}

export default Tickets