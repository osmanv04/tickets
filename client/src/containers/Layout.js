import React from 'react'
import { Layout, Menu, Breadcrumb, Col, Row } from 'antd';
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../store/actions/auth'
const { Header, Content, Footer } = Layout;

class CustomLayout extends React.Component {
    
    rendeLinkLogin() {
        if(!this.props.isAuthenticated){
            let login = 
                <Menu.Item key="1">
                    <Link to="/login">Iniciar sesión</Link>
                </Menu.Item>;
            return login;
        }
        else{
            return null;
        }
        
    }

    renderLinkSingup(){
        if(!this.props.isAuthenticated){
            let singup = 
                <Menu.Item key="2">
                    <Link to="/signup">Registrarse</Link>
                </Menu.Item>;
            return singup;
        }
        else{
            return null;
        }
        
    }

    renderLinkLogout(){

        if(this.props.isAuthenticated){
            let logout = 
                <Menu.Item key="3" onClick={this.props.logout}>
                    Cerrrar Sesión
                </Menu.Item>;
            return logout;
        }
        else{
            return null;
        }
        
    }

    renderLinkTicket(){
        if(this.props.isAuthenticated){
            let ticket = 
                <Menu.Item key="4">
                    <Link to="/tickets">Tickets</Link>
                </Menu.Item>;
            return ticket;
        }
        else{
            return null;
        }
        
    }

    render(){
        return (
            <div>
                
                <Layout  className="layout">
                    <Header style={{backgroundColor:'white'}}>
                        <div className="logo" />
                        <Row>
                            <Col span={12} offset={6}>
                                <Menu mode="horizontal" defaultSelectedKeys={['2']}>
                            
                                    {this.renderLinkLogout()}
                                    {this.rendeLinkLogin()}
                                    {this.renderLinkSingup()}
                                    {this.renderLinkTicket()}
                                                
                                </Menu>
                            </Col>
                        </Row>
                        
                        
                    </Header>
                    <Row>
                        <Col span={12} offset={6}>
                            <Content style={{ padding: '0 50px' }}>
                                <Breadcrumb style={{ margin: '16px 0' }}>
                                <Breadcrumb.Item><Link to="/">Inicio</Link></Breadcrumb.Item>
                                <Breadcrumb.Item><Link to="/">Lista de tickets</Link></Breadcrumb.Item>
                                </Breadcrumb>
                                <div className="site-layout-content">
                                    {this.props.children}
                                </div>
                        </Content>
                        </Col>
                    </Row>
                    
                   
                </Layout>
                <Footer style={{ textAlign: 'center' }}>Ticket Center 2020</Footer>
                </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}
  
export default withRouter(connect(null, mapDispatchToProps)(CustomLayout))