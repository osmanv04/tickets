import React from 'react';
import Tickets from '../components/Ticket';
import axios from 'axios';

class TicketList extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            tickets: [],
            id_type_user: null,
            is_user: null
        }
    }
   

    componentDidMount(){

        this.setState({
            id_type_user: parseInt(localStorage.getItem('idTypeUser')),
            is_user: parseInt(localStorage.getItem('id'))
        })
        
       if(parseInt(localStorage.getItem('id')) === 1){ // admin
        axios.get('http://localhost:8080/api/ticket/findByTicketOrderNull/')
            .then(resp => {
                this.setState({
                    tickets: resp.data.tickets
                })
            })
       }
       else{ // user
        if(parseInt(localStorage.getItem('id')) != null){
            axios.get('http://localhost:8080/api/ticket/findByTicketUserId/'+parseInt(localStorage.getItem('id')))
                .then(resp => {
                    this.setState({
                        tickets: resp.data.tickets
                    })
                })
            }
        }
    }

    render () {
        return (
            <Tickets bordered data={this.state.tickets} id={this.state.id_type_user}/>
        );
    }
}


export default TicketList