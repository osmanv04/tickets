import React from 'react';
import { Form, Input, Button } from 'antd';
import axios from 'axios'

class TicketForm extends React.Component {

    handleFormSubmit = (values, id) => {
        axios.post(`http://localhost:8080/api/ticket/update/`, {
                name: values.name,
                id: id
            })
            .then(res => {
                this.props.history.push('/tickets');
            })
            .catch(error => console.err(error))
    }

    render () {
        return (
            <div>
                <Form name="basic" onFinish={ (event) => this.handleFormSubmit(
                    event,
                    this.props.match.params.id
                )}>
                    <Form.Item name="name" label="Nombre">
                        <Input style={{borderRadius: '25px'}} placeholder="Agregue un nombre" />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" shape = "round" htmlType="submit">Guardar</Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export default TicketForm