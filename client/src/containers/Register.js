import React from 'react';
import {
  Form,
  Input,
  Button,
  Row,
  Col
} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import * as actions from '../store/actions/auth'
import { connect } from 'react-redux'

class Registration extends React.Component {

  onFinish = (values) => {
    this.props.onAuth(
      values.username,
      values.email,
      values.password,
      values.confirm
    )
    this.props.history.push('/')
  };


  render(){

    return (
      <div>
        <Row>
          <Col span={12} offset={6}>
          <Form
          name="register"
          onFinish={this.onFinish}
          initialValues={{
            residence: ['zhejiang', 'hangzhou', 'xihu'],
            prefix: '86',
          }}
          scrollToFirstError
        >
          <Form.Item
              name="username"
              rules={[
              {
                  required: true,
                  message: 'Ingrese su nombre de usuario',
              },
              ]}
          >
              <Input style={{borderRadius: '25px'}}  placeholder="Usuario" />
          </Form.Item>

          <Form.Item
            name="email"
            rules={[
              {
                type: 'email',
                message: 'No es una dirección válida',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input style={{borderRadius: '25px'}} placeholder="Correo electrónico" />
          </Form.Item>
    
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Ingrese su contraseña',
              },
            ]}
            hasFeedback
          >
            <Input.Password type="password"
                            placeholder="Password" style={{borderRadius: '25px'}}/>
          </Form.Item>    
          <Form.Item>
              <Button type="primary" shape="round" htmlType="submit" className="login-form-button">
                Registrarse
              </Button>
                o 
              <NavLink 
                style={{marginRight: '10px'}} 
                to='/login/'> Iniciar Sesión
              </NavLink>
          </Form.Item>

        </Form>
          </Col>
        </Row>
        
      </div>
    );

  };

  
};

const mapStateToProps = (state) => {
  return {
      loading: state.loading,
      error: state.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
      onAuth: (username, email, password1, password2) => dispatch(actions.authSignup(username, email, password1, password2))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration)