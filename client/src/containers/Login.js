import React from 'react'
import { Form, Input, Button, Spin,Row,Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import { LoadingOutlined } from '@ant-design/icons';
import { connect } from 'react-redux'
import * as actions from '../store/actions/auth'

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

class Login extends React.Component{

  onFinish = (values) => {
    this.props.onAuth(values.username, values.password)
    this.props.history.push('/')
  };

  render(){

    let errorMessage = null
    if(this.props.error){
        errorMessage = (
            <p>{this.props.error.message}</p>
        )
    }

    return (
        <div>
            <Row>
                <Col span={12} offset={6}>
                {errorMessage}
            {
                this.props.loading ?
  
                  <Spin indicator={antIcon} />
  
                  :
  
                  <Form
                      name="normal_login"
                      className="login-form"
                      initialValues={{
                          remember: true,
                      }}
                      onFinish={this.onFinish}
                      >
                      <Form.Item
                          name="username"
                          rules={[
                          {
                              required: true,
                              message: 'Por favor ingrese su usuario',
                          },
                          ]}
                      >
                          <Input style={{borderRadius: '25px'}}  placeholder="Username" />
                      </Form.Item>
                      <Form.Item
                          name="password"
                          rules={[
                          {
                              required: true,
                              message: 'Ingrese su contraseña',
                          },
                          ]}
                      >
                          <Input
                          type="password"
                          placeholder="Password" style={{borderRadius: '25px'}}
                          />
                      </Form.Item>
          
                      <Form.Item>
                          <Button type="primary"  shape="round" htmlType="submit" className="login-form-button">
                          Iniciar sesión
                          </Button>
                           Or 
                          <NavLink 
                            style={{marginRight: '10px'}} 
                            to='/signup/'> Iniciar sesión
                          </NavLink>
                      </Form.Item>
                  </Form>
            }
                </Col>
            </Row>
            
              
          </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error: state.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.authLogin(username, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)