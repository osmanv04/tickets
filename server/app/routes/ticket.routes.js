const { verifySignUp } = require("../middleware");
const controller = require("../controllers/ticket.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/ticket/create", controller.create);
  app.post("/api/ticket/update", controller.update);

  app.get("/api/ticket/findByTicketOrderNull", controller.findByTicketOrderNull);
  app.get("/api/ticket/findByTicketUserId/:id", controller.findByTicketUserId);
};
