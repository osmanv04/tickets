const db = require("../models");
const Ticket = db.ticket;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  
  Ticket.create({
      ticket_order: req.body.name,
      id_user: req.body.id
    }).
    then(() => {
      res.send({ message: "Creado!" });
    });
};

exports.update = (req, res) => {
    Ticket.update({
        ticket_order: req.body.name,
      }, {
        where: {
          id: {
            [Op.eq]: req.body.id
          }
        }
      }).
      then(() => {
        res.send({ message: "Editado!" });
      });
};

exports.findByTicketOrderNull = (req, res) => {
    Ticket.findAll({
        include: [{
            model: db.user,
            as: 'User'
        }],
        where: {
          ticket_order: {
            [Op.is]: null
          },
          id_user: {
            [Op.not]: null
          }
        }
      }).
      then(tickets => {
        res.send({ tickets: tickets });
      });
};

exports.findByTicketUserId = (req, res) => {
  Ticket.findAll({
      where: {
        id_user: req.params.id
      }
    }).
    then(tickets => {
      res.send({ tickets: tickets });
    });
};