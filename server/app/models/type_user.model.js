module.exports = (sequelize, Sequelize) => {
    const TypeUser = sequelize.define("type_users", {
      name: {
        type: Sequelize.STRING
      }
    });
  
    return TypeUser;
  };
  