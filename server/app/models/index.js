const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.type_user = require("../models/type_user.model.js")(sequelize, Sequelize);
db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.ticket = require("../models/ticket.model.js")(sequelize, Sequelize);

db.user.hasMany(db.ticket, {
  as: 'Ticket',
  foreignKey: 'id_user'
});
db.type_user.hasOne(db.user, {
  as: 'User',
  foreignKey: 'id_type_user'
});
db.ticket.belongsTo(db.user, {
  as: 'User',
  foreignKey: 'id_user'
});


module.exports = db;
