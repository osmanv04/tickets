const User = require("./user.model.js");

module.exports = (sequelize, Sequelize) => {
    const Ticket = sequelize.define("tickets", {
      ticket_order: {
        type: Sequelize.STRING,
        allowNull: true
      }
    });
  
    return Ticket;
  };
  