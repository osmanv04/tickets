const TypeUser = require("./type_user.model.js");

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    name: {
      type: Sequelize.STRING
    },
    mail: {
      type: Sequelize.STRING
    },
    pass: {
      type: Sequelize.STRING
    }
  });

  return User;
};
